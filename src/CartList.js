import React from 'react';
import './CartList.css';
import CartItem from "./CartItem";

class CartList extends React.Component{
    render() {
        let cartItems = [
            {code:"P01", name: "Traditional Merlot", description: "A bottle of middle weight cart, lower in tannins (smoother), with a more red-fruited flavor profile."},
            {code:"P02", name: "Classic Chianti", description: "A medium-bodied cart characterized by a marvelous freshness with a lingering, fruity finish"},
        ];
        let cartComponents = [];
        for (let cartItem of cartItems) {
            cartComponents.push(<CartItem item={cartItem}/>);
        }
        return <ul>{cartComponents}</ul>;
    }
}

export default CartList;
