import React from 'react';
import CartList from "./CartList";

class Cart extends React.Component{
    render() {
        return (
            <div><h2>Cart</h2><CartList/></div>
        );
    }
}

export default Cart;
