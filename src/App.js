import React from 'react';
import './App.css';
import Catalog from "./Catalog";
import Cart from "./Cart";

function App() {
  return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">The Catalog App</h1>
        </header>
        <Catalog />
          <header className="App-header">
              <h1 className="App-title">The Cart App</h1>
          </header>
          <Cart />
      </div>
  );
}

export default App;
